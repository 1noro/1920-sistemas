
# Ejercicio: El Ábaco
- Investigar sobre el Ábaco.
    - Historia y funcionamiento (operaciones).
### Requisitos
- Manuscrito de máximo 2 páginas.
- Citar como mínimo 2 fuentes bibliográficas.
    - URL de la web y la fecha de consulta.

# Información recopilada
### Posible introducción
Es uno de los instrumentos de cálculo más antiguos, utilizado especialmente por las culturas orientales.  El término procede del griego abax que significa tabla o superficie plana cubierta de polvo,  que a su vez procede del hebreo abaq que significa polvo.

Un tablero cubierto por una capa de arena (polvo) permitía trazar con facilidad dibujos y cantidades, así como su borrado.  En esta "pizarra de mano" se podían trazar surcos paralelos, en cada uno de los cuales se desplazaban cuentas (pequeñas piedras). Este parece ser el origen de esta herramienta de cálculo, aunque no sepamos quienes fueron sus inventores.

Es un instrumento de cálculo que utiliza cuentas que se deslizan a lo largo de una serie de alambres o barras fijadas a un marco para representar las unidades, decenas, centenas, etc. Probablemente de origen babilónico, es el precursor de la calculadora digital moderna. Utilizado por mercaderes en la Edad Media a través de toda Europa y el mundo árabe, fue reemplazado en forma gradual por la aritmética basada en los números indo-árabes. Aunque poco usado en Europa después del siglo XVIII, todavía se emplea en Medio Oriente, China y Japón.
> Página web, [Ábaco y su historia](https://www.monografias.com/trabajos89/abaco-y-su-historia/abaco-y-su-historia.shtml), consultado el 2019-09-18.

### Definición 1
Ábaco. Instrumento utilizado para realizar cálculos aritméticos. Suele consistir en un tabñero o cuadro con alambres o surcos paralelos entre sí, en los que se  mueven bolas o cuentas.
> Rubí, Leonel Vidales (2003), [Glosario de términos financieros: términos financieros, contables, administrativos, económicos, computacionales y legales](https://books.google.es/books?id=Z_Eyqx6XPqYC&pg=PA11&dq=%C3%A1baco+instrumento&hl=es&sa=X&ved=0ahUKEwjJrqrMl8feAhUOqIsKHSnsAn4Q6AEIQzAF#v=onepage&q=%C3%A1baco%20instrumento&f=false), consultado el 2019-09-18.

### Definición RAE
1. m. Del latín abăcus, y este del griego ἄβαξ ábax. Bastidor con cuerdas o alambres paralelos y en cada uno de ellos bolas móviles, usado para operaciones elementales de aritmética.
> Diccionario de la lengua española, Edición del Tricentenario, Actualización 2018, [Ábaco](https://dle.rae.es/?id=00NXKXl), consultado el 2019-09-18.

### Historia 1
El origen e historia del ábaco se remonta a miles de años atrás. El ábaco ha sido utilizado en diferentes formas, siendo difícil determinar su origen exacto, pero al menos tiene 3000 de antigüedad. El ábaco es considerado el dispositivo más antiguo utilizado para realizar operaciones aritméticas.

Según algunas fuentes, el ábaco se origino en Madagascar, donde había que contar a los soldados al pasar a través de un paso estrecho, por cada soldado se debía colocar una piedra en un surco cavado en el suelo.  Por cada diez soldados se creaba una segunda ranura, el «surco decenas». Después de 100 soldados, se creaba una tercera ranura, el «surco cientos» y así sucesivamente. Con esto se podía saber calcular la cantidad de municiones necesarias en una batalla.

Según otras fuentes, es la ábaco se origino en Asia Central. Donde se cree que el origen del ábaco fue en algún lugar de la ex Unión Soviética. El abaco Ruso de conoce como Stschoty. A partir de ahí, se extendió a los países vecinos.

La antigua Roma utilizaba el ábaco para contar, el ábaco romano consistía de una placa de metal con un cierto número de ranuras paralelas, dentro de las cuales se desplazaban botones de metal llamados cálculos. Este tipo de ábaco desapareció antes o con la caída del Imperio Romano.

Historiadores encontraron comentarios de Herodoto (484-425 a.C.), hablando sobre los métodos de conteo de los egipcios y griegos: «Los egipcios mueven su mano de derecha a izquierda en los cálculos, mientras los griegos lo hacen de izquierda a derecha».

En Europa, durante la Edad Media los europeos tuvieron los primeros contactos con la cultura islámica por medio de las Cruzadas (1095-1270 DC). De esta manera, los números arábigos llegaron a los métodos de cálculo del Occidente.

Posteriormente, se produjo una disputa entre los partidarios de los métodos del ábaco y los que preferían el método árabe para la escritura de los números. Todo el mundo estaba convencido de que su método era mejor. La iglesia, que en ese momento tenía una enorme influencia en la filosofía y la ciencia vio la toma de posesión de los números árabes como una amenaza a su propia autoridad, declarando el método árabe como de la mano del diablo.

El uso del ábaco se mantuvo hasta el siglo 18. Los funcionarios británicas utilizaban el ábaco para sus cálculos fiscales. La Revolución Francesa (desde 1789) prohibió el uso del ábaco en las escuelas y las administraciones, para basarse en el sistema árabe, independientemente del ábaco.

En China, antes de la introducción de ábaco (suan pan) se realizaban cálculos utilizando  un método más complicado de de líneas verticales y horizontales. El sistema fue escrito en la época de la dinastía Ming (1368-1646), siendo reemplazado por otros más eficaces suan pan (ábaco chino). El ábaco chino se propagó hacia Corea en el 1400 y en el Japón (soroban nombre del ábaco japonés) en el 1600, así como al sureste de Asia.

En América, el imperio incaico utilizó otra modalidad de ábacos  que tenían como nombre «quipus».  Otra modalidad de ábaco es el azteca o Nepohualtzitzin fabricado con madera, hilos y granos de maíz, desapareció después de la conquista de México en 1521.
> Página web, [¿Cuál es el Origen y la Historia del Abaco?](http://www.cavsi.com/preguntasrespuestas/cual-es-el-origen-y-la-historia-del-abaco/), consultado el 2019-09-18.

### Historia 2
El ábaco es considerado como el más antiguo instrumento de cálculo, adaptado y apreciado en diversas culturas. El origen del ábaco está literalmente perdido en el tiempo. En épocas muy tempranas el hombre primitivo encontró materiales para idear instrumentos de conteo. Es probable que su inicio fuera una superficie plana y piedras que se movían sobre líneas dibujadas con polvo. Hoy en día se tiende a pensar que el origen del ábaco se encuentra en China, donde el uso de este instrumento aún es notable al igual que en Japón.

Debido a que gran parte de la aritmética se realizaba en el ábaco, el término ábaco ha pasado a ser sinónimo de aritmética; encontramos tal denominación en Leonardo de Pisa Fibbonacci (1170-1250) en su libro "Liber Abaci" publicado en 1202, que trata del uso de los números indo-arábigos.

Muchas culturas han usado el ábaco o el tablero de conteo, aunque en las culturas europeas desapareció al disponerse de otros métodos para hacer cálculos, hasta tal punto que fue imposible encontrar rastro de su técnica de uso. Las evidencias del uso del ábaco surgen en comentarios de los antiguos escritores griegos. Por ejemplo, Demóstenes (384-322 a.C.) escribió acerca de la necesidad del uso de piedras para realizar cálculos difíciles de efectuar mentalmente.

Otro ejemplo son los métodos de cálculo encontrados en los comentarios de Herodoto (484-425 a.C.), que hablando de los egipcios decía: "Los egipcios mueven su mano de derecha a izquierda en los cálculos, mientras los griegos lo hacen de izquierda a derecha".

Algunas de las evidencias físicas de la existencia del ábaco se encontraron en épocas antiguas de los griegos en las excavaciones arqueológicas. En 1851 se encontró una gran ánfora de 120 cm. de altura, a la que se denominó "Vaso de Darío" y entre cuyos dibujos aparece una figura representando un contador que realiza cálculos manipulando cuentas. La segunda muestra arqueológica es un auténtico tablero de conteo encontrado en 1846 en la isla de Salamis; el tablero de Salamis, probablemente usado en Babilonia 300 A.C., es una gran pieza de mármol de 149 cm. de largo por 75 cm. de ancho, con inscripciones que se refieren a ciertos tipos de monedas de la época; este tablero está roto en dos partes.

Este dispositivo en la forma moderna en que la conocemos, realmente apareció en el siglo 13 DC y sufrió varios cambios y evoluciones en su técnica de calcular. Actualmente está compuesto por 10 columnas con 2 bolitas en la parte superior 5 en la parte inferior.

Los japoneses copiaron el ábaco chino y lo rediseñaron totalmente a 20 columnas con 1 bolita en la parte superior y 10 en la inferior, denominándolo Soroban.

Como un evento anecdótico muy importante del uso y la potencia del ábaco fue que el 12 de noviembre de 1946, una competencia, entre el japonés Kiyoshi Matsuzaki del Ministerio Japonés de comunicaciones utilizando un ábaco japonés y el estadounidense Thomas Nathan Wood de la armada de ocupación de los Estados Unidos con una calculadora electromecánica, fue llevada a cabo en Tokyo, bajo patrocinio del periódico del ejército estadounidense (U.S. Army), Stars and Stripes. Matsuzaki utilizando el ábaco japonés resultó vencedor en cuatro de las cinco pruebas, perdiendo en la prueba con operaciones de multiplicación.

El 13 de noviembre de 1996, los científicos Maria Teresa Cuberes, James K. Gimzewski, y Reto R. Schlittler del laboratorio de IBM de Suiza de la división de investigación, construyeron un ábaco que utiliza como cuentas moléculas cuyo tamaño es inferior a la millonésima parte del milímetro. El "dedo" que mueve las cuentas moleculares es un microscopio de efecto túnel.

Actualmente el antiguo ábaco se emplea como método de enseñanza en las escuelas de los países orientales, aunque es usado regularmente en muchos lugares del mundo, particularmente en los pequeños negocios de los barrios chinos (Chinatown) en los Estados Unidos de América, Canadá y países cosmopolitas.
> Página web, [Ábaco y su historia](https://www.monografias.com/trabajos89/abaco-y-su-historia/abaco-y-su-historia.shtml), consultado el 2019-09-18.

### Uso del ábaco
Se compone de una serie de hileras formadas por una serie de cuentas insertadas en una varilla por la que pueden deslizarse libremente, representando de esa manera un número del 0 al 9.
- La primera hilera de la derecha corresponde a las unidades, la segunda a las decenas, la tercera a las centenas y así sucesivamente. Cada una de estas hileras se halla dividida en dos mitades:
- La inferior tiene cinco cuentas y cada vez que cada una es desplazada hacia la división central representa una unidad.
- La superior sólo tiene dos cuentas y cada vez que una de ellas es desplazada hacia la división central viene a representar cinco unidades.
> Página web, [Ábaco y su historia](https://www.monografias.com/trabajos89/abaco-y-su-historia/abaco-y-su-historia.shtml), consultado el 2019-09-18.

### Operaciones
- Suma.
- Resta.
- Multiplicación.
- División.
- Raices cuadradas.
- Raices cúbicas.
> Página web, [Operaciones fundamentales en la aritmética del Ábaco Chino](http://www.librosmaravillosos.com/swanpan/index.html), consultado el 2019-09-18.

# Referencias sin revisar
- ...
