
# Ejercicio: Herman Hollerith
- Pequeña biografía.
- Máquina tabuladora (o censadora).
### Requisitos
- Manuscrito de máximo 1 página.
- Citar como mínimo 2 fuentes bibliográficas.
    - URL de la web y la fecha de consulta.

# Biografía
(Herman o Hermann Hollerith; Buffalo, 1860 - Washington, 1929) Estadístico estadounidense considerado uno de los pioneros de la informática por su invención de las máquinas estadísticas de tarjetas o fichas perforadas, con las que logró automatizar los trabajos de cómputo y clasificación de grandes volúmenes de información.

Tras licenciarse en 1879 por la Universidad de Columbia, inició su actividad profesional en la Oficina Nacional del Censo. En aquella época el censo estadounidense se realizaba cada diez años, y el cúmulo de datos recogido era tal que, al iniciarse un nuevo censo, todavía no habían llegado a procesarse todos los datos del censo anterior. Herman Hollerith ideó una cinta de papel en la que los datos se señalaban practicando un agujero; los agujeros de la cinta podían ser luego leídos por un dispositivo electromecánico, lo que permitía acelerar sensiblemente el procesamiento de los datos.

A lo largo de la década de 1880, Hollerith probó con éxito su invento en diversas instituciones públicas y se aplicó a perfeccionarlo; la principal mejora fue sustituir la cinta de papel por una serie de tarjetas perforadas, sistema que patentó en 1889. Ese mismo año, Hollerith sometió a la consideración del gobierno de los Estados Unidos un proyecto para construir una máquina estadística de fichas perforadas que fue finalmente utilizada para computar los datos del censo de 1890. La máquina tabuladora de Hollerith fue capaz de procesar los datos de los 60 millones de ciudadanos estadounidenses en menos de tres años.

Hollerith continuó introduciendo mejoras y diseñando nuevas máquinas, y en 1896 fundó la Tabulating Machine Company, empresa dedicada a la fabricación y comercialización de máquinas procesadoras de datos. Esta empresa pasó a llamarse en 1924 International Business Machines (IBM), y se convertiría tras la Segunda Guerra Mundial en una de las compañías punteras del sector informático.
> Página web, [Biografía de Herman Hollerith](https://www.biografiasyvidas.com/biografia/h/hollerith.htm), consultado el 2019-09-24.

# Máquina tabuladora

### Descripcción 1
La primer idea de Hollerith fue la de codificar información en cinta de papel. La cinta estaba dividida en “campos” mediante marcas de tinta. Cada campo representaba diferentes categorías: por ejemplo, varón o mujer, blanco o negro. La presencia de un agujero en el campo varón/mujer significaba que la persona era un varón, mientras que su ausencia implicaba que se trataba de una mujer, y así sucesivamente. Estos agujeros después se podían “leer” mediante una máquina. Sus primeras patentes se produjeron en 1884 y dedicó los años siguientes a perfeccionar el sistema. Empezó por procesar la información relativa a las estadísticas de sanidad de las ciudades norteamericanas y de la administración militar.

El censo de 1880 había demandado 7 años de análisis, y según las proyecciones de aumento poblacional, el censo de 1890 implicaría más de 10 años de tabulación y cálculo manual. Así, Hollerith comenzó a trabajar en el diseño de una máquina tabuladora o censadora que permitiera reducir el tiempo de análisis de datos, buscando mecanizar la tabulación manual.

Hollerith observó que la mayor parte de las preguntas contenidas en los censos se podían contestar con opciones binarias: SÍ o NO, abierto o cerrado. Entonces ideó una tarjeta perforada, una cartulina compuesta por 80 columnas con 2 posiciones, con la cual se contestaba este tipo de preguntas. Esta noción de programación binaria había sido usada ya en 1801 por el inventor francés Joseph Marie Jacquard.

El método original de Hollerith para representar la información se sigue utilizando, un siglo después, si bien se ha modificado el formato de la tarjeta. Las modernas tarjetas perforadas constan de 12 filas y 80 columnas. Las máquinas tabuladoras utilizaban el sistema decimal y, por tanto, cada ficha podía almacenar 80 números. Los caracteres alfabéticos se crearon con la “multiperforación”: efectuando más de un agujero en una columna. Los ordenadores también aceptan fichas perforadas en sistema binario.

Cinco años después, en 1889, perfeccionó la idea de la cinta de papel utilizando tarjetas separadas para cada persona. Las tarjetas eran del tamaño de los billetes de un dólar; se dice que esto se debió, en parte, a que los únicos equipos que se pudieron adaptar habían sido construidos para manipular dinero. Originalmente los agujeros eran redondos y se hacían con el punzón que utilizaban los conductores de ómnibus para perforar los pasajes, pero luego se construyeron punzones especiales para cortar un agujero cuadrado de 6 mm. Así se podía incluir gran cantidad de información en una sola tarjeta.

El éxito comercial llegó en ese mismo 1889, cuando el Bureau of Censuses (Departamento de Censos) convocó un concurso para proveerse de un sistema que procesara el censo del  año siguiente. Los sistemas se probaron volviendo a tabular los datos del censo anterior. La convocatoria la ganó el equipo de Hollerith. Para entonces todas sus máquinas estaban amparadas bajo patentes y él  aprovechó su monopolio para cargarle al gobierno 65 céntimos por cada mil tarjetas procesadas. Aunque cada habitante de Estados Unidos tenía su propia tarjeta, Hollerith sólo tardó dos años en hacer el trabajo. Anunció que la población del país era de 56 millones de habitantes y presentó la factura al gobierno.

En 1890, el Gobierno estadounidense eligió la máquina tabuladora (considerada como la primera computadora) de Hollerith para elaborar el censo. Con este método, el resultado del recuento y análisis censal de los 62,622,250 habitantes estuvo listo en sólo 6 semanas.

Cuando llegó el momento de realizar el censo de 1900, Hollerith había desarrollado una maquinaria más eficaz, pero se negó a rebajar su tarifa. Cuando caducaron sus patentes, el gobierno buscó otras empresas, pero Hollerith superó a la competencia fundando su propia compañía, que luego se convertiría en la IBM (Internacional Business Machines)

La ventaja que ofrecen las tarjetas individuales sobre la cinta contínua es que la información se puede clasificar. Por ejemplo, a usted tal vez le interse saber qué cantidad de mujeres blancas de 80 años de edad viven en la ciudad de Nueva York. Se podrían clasificar todas las tarjetas y las que tuvieran agujeros perforados en estos tres campos se podrían separar mecánicamente del resto.

Estas primeras máquinas sólo podían producir totales, pero posteriormente Hollerith introdujo  la suma y otras operaciones aritméticas sencillas.

En 1890, el Gobierno estadounidense eligió la máquina tabuladora (considerada como la primera computadora) de Hollerith para elaborar el censo.

Hollerith patentó su máquina en 1889, que es sólo una dentro de sus más de treinta patentes. Años después, en 1896, Hollerith fundó la empresa Tabulating Machine Company, con el fin de explotar comercialmente su invento. En 1911, dicha compañía se fusionó con Dayton Scale Company, International Time Recording Company y Bundy Manufacturing Company, para crear la Computing Tabulating Recording Company (CTR). El 14 de febrero de 1924, CTR cambió su nombre por el de International Business Machines Corporation (IBM), cuyo primer presidente fue Thomas John Watson.

En 1906 Hollerith añadió a su máquina un panel de control, lo que la convirtió en una máquina capaz de hacer varios trabajos sin necesidad de reconstruirla, hasta entonces todas las máquinas estaban hechas a propósito, y no de propósito general. Éste paso se dice que fue uno de los primeros hacia la programación.

Para 1910 la Oficina del Censo de EEUU ya tenía preparada supropia máquina, pero no se sabe porqué se dejó la patente de esta a James Powers, quien pronto crearía su propia compañía (más adelante absorbida por la Remington Rand) y desbancaría del monopolio a Hollerith. Ésto, acompañado de una serie de fallos en sus máquinas para las ferroviarias evitando que estas quisieran continuar sus contratos, le llevó a fusionarse con otras compañías para sobrevivir. El fruto de esta fusión fue la Computing Tabulating Recording Company, donde permaneció de asesor hasta que se retiró en 1921. Paso el resto de sus días en una granja dedicando su vida la cría de ganado Guernsey. Mientras tanto la CTR cambió su nombre a IBM.

El 17 de Noviembre de 1929 muere de un infarto al corazón.
> Página web, [Hollerith y la Maquina Tabuladora](https://sites.google.com/site/cantaliciohollerithoa/home/maquina-tabuladora), consultado el 2019-09-24.

# Referencias sin revisar
- ...
