@ECHO OFF
CHCP 1250 > NUL
TITLE Backup
CLS
ECHO Este batch permite copiar � carpeta C:\backup
ECHO os arquvos da extensi�n seleccionada polo usuario
ECHO.

SET ext=
:BUCLE
SET /P ext=Indique a extensi�n dos arquivos a copiar: 
IF [%ext%]==[] (
	ECHO Ten que introducir a extensi�n dos arquivos que quere copiar
	GOTO BUCLE
)

IF NOT EXIST C:\backup (MKDIR C:\backup) 

ECHO Copia realizada o d�a %date% �s %time% > backup.log
ECHO. >> backup.log
ECHO Arquivos copiados >> backup.log
ECHO ----------------------------------------------- >> backup.log

COPY *.%ext% C:\backup >> backup.log 

ECHO. >> backup.log
ECHO Arquivos no directorio da copia de seguridade >> backup.log
ECHO ----------------------------------------------- >> backup.log
DIR /B C:\backup >> backup.log
ECHO ----------------------------------------------- >> backup.log

ECHO.	
ECHO Copia de seguridade rematada

:FIN
TITLE S�mbolo del sistema
CHCP 850 > NUL
