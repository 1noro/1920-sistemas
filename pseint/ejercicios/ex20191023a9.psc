// palindromo ex20191023a

// frase_ori <- frase
// out -> es palindromo o no

// ignorar acentos, mayusculas y espacios

// funcion util: concatenar()
// HACIENDO EL REVERSE
Proceso palindromo
	
	Definir frase_ori, frase_limpia Como Cadena
	Definir c Como Caracter
	Definir i Como Entero
	
	Repetir
		
		Escribir "Escribe una frase sin signos de puntuaci�n."
		Escribir "Frase " Sin Saltar
		Leer frase_ori
		
		Si frase_ori <> "" Entonces
			
			frase_limpia <- ""	
			
			Para i <- 0 Hasta Longitud(frase_ori) - 1 Con Paso 1 Hacer
				
				c <- Minusculas(SubCadena(frase_ori, i, i))
				
				Segun c Hacer
					"�":
						c <- "a"
					"�":
						c <- "e"
					"�":
						c <- "i"
					"�":
						c <- "o"
					"�", "�":
						c <- "u"
					" ":
						c <- ""
				FinSegun
				
				frase_limpia <- Concatenar(frase_limpia, c)
			FinPara

			frase_ori <- ""
			Para i <- Longitud(frase_limpia) - 1 Hasta 0 Con Paso (-1) Hacer
				frase_ori <- Concatenar(frase_ori, SubCadena(frase_limpia, i, i))
			FinPara

			Si frase_limpia = frase_ori Entonces
				Escribir "[ OK ] Es pal�ndromo :D"
			SiNo
				Escribir "[FAIL] NO es pal�ndromo :("
			FinSi
			
		FinSi
	Hasta Que frase_ori = ""
	
FinProceso
