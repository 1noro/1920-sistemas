// pares ex20191010e

// para
// in <- 2 num entre [1, 100]
// presenta os nuemros pares entre eles
// usar repetir para validar
Proceso pares
	Definir a, b, i Como Entero
	
	Repetir
		Escribir "MENOR entre [1, 99] " Sin Saltar
		Leer a
		Escribir "MAIOR entre [", a + 1, ", 100] " Sin Saltar
		Leer b
	Hasta Que (a >= 1) Y (b <= 100) Y (a < b)
	
	Escribir "# N�meros pares:" Sin Saltar
	
	Para i <- a Hasta b Con Paso 1 Hacer
		Si ((i MOD 2) == 0) Entonces
			Escribir " ", i Sin Saltar
		FinSi
	FinPara
	
	Escribir ""
	
FinProceso
