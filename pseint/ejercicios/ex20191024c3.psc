// matrices3 ex20191024c

// in <- 10 num en [1, 99]
Proceso matrices3
	
	Definir n, long, i, e, in Como Entero
	Definir duplicado Como Logico
	long <- 10
	Dimension n[long]
	
	i <- 0
	Repetir
		duplicado <- Falso
		Escribir "Escribe el n�mero ", i + 1, " en [1, 999] " Sin Saltar
		Leer in
		
		Si in >= 1 Y in <= 999 Entonces
			
			Si i <> 0 Entonces
				e <- 0
				Repetir
					Si in = n[e] Entonces
						duplicado <- Verdadero
					FinSi
					e <- e + 1 
				Hasta Que e >= i O duplicado
			FinSi
			
			Si duplicado Entonces
				Escribir "N�mero dupliado."
			SiNo
				n[i] <- in
				i <- i + 1
			FinSi
			
		SiNo
			Escribir "N�mero fuera de rango."
		FinSi
		
	Hasta Que i = long
	
	// PRESENTAR RESULTADOS
	Escribir "Vector: [" Sin Saltar
	Para i <- 0 Hasta long - 1 Con Paso 1 Hacer
		Si i <> (long - 1) Entonces
			Escribir n[i], ", " Sin Saltar
		SiNo
			Escribir n[i], "]"
		FinSi
	FinPara
	
FinProceso
