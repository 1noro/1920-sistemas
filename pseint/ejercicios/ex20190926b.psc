// ordenarNumeros ex20190926b

// Estructura selectiva dobre
// 2 numeros de entrada orden ascendente
Proceso ordenarNumeros
	Definir a, b Como Entero
	Escribir "Primer n�mero " Sin Saltar
	Leer a
	Escribir "Segundo n�mero " Sin Saltar
	Leer b
	
	Si a < b Entonces
		Escribir a, ", ", b
	SiNo
		Escribir b, ", ", a
	FinSi
	
FinProceso
