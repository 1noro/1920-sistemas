// sumaNumeros ex20191010b

// repetir
// calcular la suma de 2 valores hasta que los 2 numeros sean 0
Proceso sumaNumeros
	Definir a, b Como Entero
	
	Repetir
		Escribir "N�mero 1 " Sin Saltar
		Leer a
		Escribir "N�mero 2 " Sin Saltar
		Leer b
		Si (a <> 0) O (b <> 0) Entonces
			Escribir a, " + ", b, " = ", a + b
		FinSi
	Hasta Que (a == 0) Y (b == 0)
	
	Escribir "Fin, de proceso."
	
FinProceso
