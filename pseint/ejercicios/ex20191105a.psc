// estaEnFibonacci ex20191105a

// comprobar si un numero esta en fibonacci en [2, 9999]
// salida 0
Proceso estaEnFibonacci
	Definir b, n1, n2, aux Como Entero
	Definir encontrado Como Logico
	
	Repetir
		
		encontrado <- falso
		
		Repetir
			Escribir "N�mero a buscar [2, 9999], 0 para salir: " Sin Saltar
			Leer b 
		Hasta Que (2 <= b Y b <= 9999) O b = 0
		
		Si b <> 0 Entonces
			n1 <- 0
			n2 <- 1
			aux <- 0
			Repetir
				aux <- n1 + n2
				n1 <- n2
				n2 <- aux
				Si b = aux Entonces
					encontrado <- Verdadero
				FinSi
//				Escribir aux
			Hasta Que aux > 9999 O encontrado
			
			Si encontrado Entonces
				Escribir "[ OK ] ", b, " pertenece a la serie de Fibonacci."
			SiNo
				Escribir "[FAIL] ", b, " NO pertenece a la serie de Fibonacci."
			FinSi
			
		FinSi

	Hasta Que b = 0
FinProceso
