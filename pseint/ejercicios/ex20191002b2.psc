// contMonedas ex20191002b

// in <- euros (decimais)
// out -> cant. min de billetes e monedas que suman esa cifra
// help: funcion matem�tica trunc() (cociente enteiro)

// 500, 200, 100, 50, 20, 10, 5
// 2, 1
// 0.5, 0.2, 0.1
// 0.05, 0.02, 0.01
Proceso contMonedas
	Definir dinero Como Real
	Definir e, d Como Entero

	Escribir "Dinero (en E) " Sin Saltar
	Leer dinero

	e <- trunc(dinero) // euros
	d <- trunc((dinero - e) * 100) // centimos * 100

	// billetes
	Escribir "## Billetes ##"
	Escribir "500: ", trunc(e / 500)
	e <- e - (trunc(e / 500) * 500)
	Escribir "200: ", trunc(e / 200)
	e <- e - (trunc(e / 200) * 200)
	Escribir "100: ", trunc(e / 100)
	e <- e - (trunc(e / 100) * 100)
	Escribir "50:  ", trunc(e / 50)
	e <- e - (trunc(e / 50) * 50)
	Escribir "20:  ", trunc(e / 20)
	e <- e - (trunc(e / 20) * 20)
	Escribir "10:  ", trunc(e / 10)
	e <- e - (trunc(e / 10) * 10)
	Escribir "5:   ", trunc(e / 5)
	e <- e - (trunc(e / 5) * 5)
	
	// monedas (E)
	Escribir "## Monedas ##"
	Escribir "2E:    ", trunc(e / 2)
	e <- e - (trunc(e / 2) * 2)
	Escribir "1E:    ", trunc(e / 1)
	e <- e - (trunc(e / 1) * 1)
	
	// monedas (cent)
	Escribir "50cnt: ", trunc(d / 50)
	d <- d - (trunc(d / 50) * 50)
	Escribir "20cnt: ", trunc(d / 20)
	d <- d - (trunc(d / 20) * 20)
	Escribir "10cnt: ", trunc(d / 10)
	d <- d - (trunc(d / 10) * 10)
	Escribir "5cnt:  ", trunc(d / 5)
	d <- d - (trunc(d / 5) * 5)
	Escribir "2cnt:  ", trunc(d / 2)
	d <- d - (trunc(d / 2) * 2)
	Escribir "1cnt:  ", trunc(d / 1)
	d <- d - (trunc(d / 1) * 1)

FinProceso
