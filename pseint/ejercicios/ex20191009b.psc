// numeroMaior ex20191009b

// mientras
// infinit. num enteiros (in)
// hasta que sea menor que el anterior
Proceso numeroMaior
	Definir n1, n2 Como Entero
	
	Escribir "num " Sin Saltar
	Leer n1
	
	Escribir "num " Sin Saltar
	Leer n2
	
	Mientras n1 < n2 Hacer
		n1 <- n2
		Escribir "num " Sin Saltar
		Leer n2
	FinMientras
	
	Escribir "Fin, de proceso."
	
FinProceso
