// palindromo ex20191023a

// in <- frase
// out -> es palindromo o no

// ignorar acentos, mayusculas y espacios

// funcion util: concatenar()
// HACIENDO EL REVERSE
Proceso palindromo
	
	Definir in, in2 Como Cadena
	Definir c Como Caracter
	Definir i Como Entero
	
	Repetir
		
		Escribir "Escribe una frase sin signos de puntuaci�n."
		Escribir "Frase " Sin Saltar
		Leer in
		
		Si in <> "" Entonces
			
			in2 <- ""
			
			Para i <- 0 Hasta Longitud(in) Con Paso 1 Hacer
				c <- SubCadena(in, i, i)
				
				Segun c Hacer
					"�":
						c <- "a"
					"�":
						c <- "e"
					"�":
						c <- "i"
					"�":
						c <- "o"
					"�":
						c <- "u"
					"�":
						c <- "u"
					"�":
						c <- "a"
					"�":
						c <- "e"
					"�":
						c <- "i"
					"�":
						c <- "o"
					"�":
						c <- "u"
					"�":
						c <- "u"
				FinSegun
				
				Si c <> " " Entonces
					in2 <- Minusculas(Concatenar(in2, c))
				FinSi
			FinPara

			in <- ""
			Para i <- Longitud(in2) - 1 Hasta 0 Con Paso (-1) Hacer
				in <- Concatenar(in, SubCadena(in2, i, i))
			FinPara

			Si in2 = in Entonces
				Escribir "[ OK ] Es pal�ndromo :D"
			SiNo
				Escribir "[FAIL] NO es pal�ndromo :("
			FinSi
			
		FinSi
	Hasta Que in = ""
	
FinProceso
