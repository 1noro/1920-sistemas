// validarFecha ex20191003b

// in <- dia, mes, ano
// 1/1/1 - 31/12/2999
// a�os visiestos: mult 4, no mult 100, mult 400
// meses:
// (31d) 1 3 5 7 8 10 12
// (30d) 4 6 9 11
// (28/29d) 2
Proceso validarFecha
	Definir d, m, a Como Entero
	Definir out Como Logico
	out <- Falso
	
	Escribir "A�o " Sin Saltar
	Leer a
	Escribir "Mes " Sin Saltar
	Leer m
	Escribir "D�a " Sin Saltar
	Leer d
	
	Si (a >= 1) Y (a <= 2999) Y (m >= 1) Y (m <= 12) Y (d >= 1) Entonces
		Segun m Hacer
			1, 3, 5, 7, 8, 10, 12:
				// 31d
				Si (d <= 31) Entonces
					out <- Verdadero
				FinSi
			4, 6, 9, 11:
				// 31d
				Si (d <= 30) Entonces
					out <- Verdadero
				FinSi
			De Otro Modo:
				// 28/29d
				Si ((a mod 4) == 0) Y ( ((a mod 100) <> 0) O ((a mod 400) == 0) ) Entonces
					Si (d <= 29) Entonces
						out <- Verdadero
					FinSi
				SiNo
					Si (d <= 28) Entonces
						out <- Verdadero
					FinSi
				FinSi
				
		FinSegun
	FinSi
	
	Si out Entonces
		Escribir "[ OK ] ", a, "-", m, "-", d, " es una fecha v�lida."
	SiNo
		Escribir "[FAIL] ", a, "-", m, "-", d, " no es una fecha v�lida."
	FinSi
	
FinProceso