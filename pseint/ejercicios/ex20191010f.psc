// cadrado1 ex20191010f

// para
// cadrado de *, con dimensións entre [2, 20]
Proceso cadrado1
	Definir a, i, e Como Entero
	
	Repetir
		Escribir "Medida en [2, 20] " Sin Saltar
		Leer a
	Hasta Que (a >= 2) Y (a <= 20)
	
	Escribir ""
	
	Para i <- 1 Hasta a Con Paso 1 Hacer
		Para e <- 1 Hasta a Con Paso 1 Hacer
			Escribir " *" Sin Saltar
		FinPara
		Escribir ""
	FinPara
	
	Escribir ""
	
FinProceso