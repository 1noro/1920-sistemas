// matrices ex20191024a

// 
Proceso matrices
	Definir nums, i Como Entero
	Dimension nums[4]
	
//	Para i <- 0 Hasta 4 - 1 Con Paso 1 Hacer
//		nums[i] <- i * 2
//	FinPara
	
	Para i <- 0 Hasta 4 - 1 Con Paso 1 Hacer
		Escribir "Posici�n ", i + 1, " " Sin Saltar
		Leer nums[i]
	FinPara
	
	Para i <- 0 Hasta 4 - 1 Con Paso 1 Hacer
		Escribir nums[i]
	FinPara
	
FinProceso
