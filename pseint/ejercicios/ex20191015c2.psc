// factorizar ex20191015c2

// estructura libre (Repetir)
// representar a descomposición factorial
Proceso factorizar
	Definir n, i, e Como Entero
	
	Repetir
		Escribir "Número en [2, 1999] " Sin Saltar
		Leer n
	Hasta Que (n >= 2) Y (n <= 1999)
	
	i <- 2
	e <- n
	
	Escribir n, " = " Sin Saltar
	
	Repetir
	    Si ((n MOD i) == 0) Entonces
	        Escribir i Sin Saltar
	        n <- n / i
			Si n > 1 Entonces
				Escribir " x " Sin Saltar
			FinSi
	    SiNo
	        i <- i + 1
	        e <- e - 1
	    FinSi
	Hasta Que e <= 0 
	
	Escribir ""
	
FinProceso
