// calculoImporte ex20190926a

// Estructura selectiva simple
// Solicitanse unidades e prezo dun produto e presentanse.
// Se se supera o valor de 100E calcularase un desconto do 5% sobre o total.
Proceso calculoImporte
	Definir unidades Como Entero
	Definir p_descuento, precio, total, descuento, total_descontado Como Real
	p_descuento <- 0.05
	
	Escribir "Introduce el n�mero de unidades: " Sin Saltar
	Leer unidades
	Escribir "Introduce el precio por unidad: " Sin Saltar
	Leer precio
	
	total <- unidades * precio
	Escribir "Unidades: ", unidades, "U, Total: ", total, "E"
	
	Si total > 100 Entonces
		descuento <- total * p_descuento
		total_descontado <- total - descuento		
		Escribir "A descontar (", p_descuento, "): ", descuento, "E, Total descontado: ", total_descontado, "E"
	FinSi
	
FinProceso
