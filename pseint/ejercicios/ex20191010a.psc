// comprobarSalida ex20191010a

// repetir
// comprobar salida de programa (s/n) en bucle
Proceso comprobarSalida
	Definir c Como Caracter
	
	Repetir
		Escribir "Salir del programa? (s/n) " Sin Saltar
		Leer c
	Hasta Que (c == 's') O (c == 'n')

	Escribir "Fin, de proceso."
	
FinProceso
