// contMonedas ex20191002b

// in <- euros (decimais)
// out -> cant. min de billetes e monedas que suman esa cifra
// help: funcion matem�tica trunc() (cociente enteiro)

// 500, 200, 100, 50, 20, 10, 5
// 2, 1
// 0.5, 0.2, 0.1
// 0.05, 0.02, 0.01
Proceso contMonedas
	Definir dinero Como Real
	Definir e Como Entero
	Definir d Como Real

	Definir b500, b200, b100, b50, b20, b10, b5 Como Entero
	Definir m2, m1 Como Entero
	Definir c50, c20, c10, c5, c2, c1 Como Entero

	b500 <- 0
	b200 <- 0
	b100 <- 0
	b50 <- 0
	b20 <- 0
	b10 <- 0
	b5 <- 0

	m2 <- 0
	m1 <- 0

	c50 <- 0
	c20 <- 0
	c10 <- 0
	c5 <- 0
	c2 <- 0
	c1 <- 0

	Escribir "Dinero (en E) " Sin Saltar
	Leer dinero

	e <- trunc(dinero)
	d <- (dinero - e) * 100

	// billetes
	Si (e >= 500) Entonces
		b500 <- trunc(e / 500)
		e <- e - (b500 * 500)
	FinSi

	Si (e >= 200) Entonces
		b200 <- trunc(e / 200)
		e <- e - (b200 * 200)
	FinSi

	Si (e >= 100) Entonces
		b100 <- trunc(e / 100)
		e <- e - (b100 * 100)
	FinSi

	Si (e >= 50) Entonces
		b50 <- trunc(e / 50)
		e <- e - (b50 * 50)
	FinSi

	Si (e >= 20) Entonces
		b20 <- trunc(e / 20)
		e <- e - (b20 * 20)
	FinSi

	Si (e >= 10) Entonces
		b10 <- trunc(e / 10)
		e <- e - (b10 * 10)
	FinSi

	Si (e >= 5) Entonces
		b5 <- trunc(e / 5)
		e <- e - (b5 * 5)
	FinSi

	// monedas (E)
	Si (e >= 2) Entonces
		m2 <- trunc(e / 2)
		e <- e - (m2 * 2)
	FinSi

	Si (e >= 1) Entonces
		m1 <- trunc(e / 1)
		e <- e - (m1 * 1)
	FinSi

	// monedas (cent)
	Si (d >= 50) Entonces
		c50 <- trunc(e / 1)
		e <- e - (m1 * 1)
	FinSi

	Escribir "## Billetes ##"
	Escribir "500: ", b500
	Escribir "200: ", b200
	Escribir "100: ", b100
	Escribir "50:  ", b50
	Escribir "20:  ", b20
	Escribir "10:  ", b10
	Escribir "5:   ", b5

	Escribir "## Monedas ##"
	Escribir "2E:    ", m2
	Escribir "1E:    ", m1
	Escribir "50cnt: ", c50
	Escribir "20cnt: ", c20
	Escribir "10cnt: ", c10
	Escribir "5cnt:  ", c5
	Escribir "2cnt:  ", c2
	Escribir "1cnt:  ", c1

FinProceso
