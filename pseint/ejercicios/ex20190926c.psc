// determinarMaioriaIdade ex20190926c

// esctructura selectiva dobre
// determinar minoria de idade a partir dunha idade numerica solicitada
// valores validos deben estar entre 0 e 125
Proceso determinarMaioriaIdade
	Definir edad Como Entero
	Escribir "Edad " Sin Saltar
	Leer edad
	Si (edad < 0) O (edad > 125) Entonces
		Escribir edad, " NO es una edad v�lida."
	SiNo
		Si edad < 18 Entonces
			Escribir "Tienes ", edad, " a�os, eres menor de edad."
		SiNo
			Escribir "Tienes ", edad, " a�os, eres mayor de edad."
		FinSi
	FinSi
FinProceso
