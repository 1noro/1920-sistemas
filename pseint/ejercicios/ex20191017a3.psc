// primo ex20191017a

// in <- n [2, 9999]
// out -> primo || no primo
// exit <- 0
Proceso detPrimo
	Definir n, i Como Entero
	Definir es_primo Como Logico
	
	Repetir
	
		Repetir
			Escribir "N�mmero en [2, 9999] " Sin Saltar
			Leer n
		Hasta Que (2 <= n Y n <= 9999) O n = 0
		
		Si n <> 0 Entonces
			
			es_primo <- Verdadero
			Para i <- 2 Hasta n - 1 Con Paso 1 Hacer
				Si (n MOD i) = 0 Entonces
					es_primo <- Falso
					i <- n
				FinSi
			FinPara
			
			Si es_primo Entonces
				Escribir n, " es primo."
			SiNo
				Escribir n, " NO es primo."
			FinSi
		FinSi
		
	Hasta Que n = 0
	
FinProceso
