// rangoNumeros ex20191009c

// mientras
// definir un rango [n1, n2]
// pedir n�meros enteros dentro de ese rango
// exit - fuera de rango
// contar numeros y sumar los del bucle
Proceso rangoNumeros
	Definir min, max, nin, suma, cuenta Como Entero
	cuenta <- 0
	suma <- 0
	
	min <- 0
	max <- 0
	
	Escribir "M�nimo " Sin Saltar
	Leer min
	
	Escribir "M�ximo " Sin Saltar
	Leer max
	
	//Mientras min >= max Hacer
	Mientras min > max Hacer
		Escribir "M�ximo " Sin Saltar
		Leer max
	FinMientras
	
	Escribir "## Introduce n�meros dentro del rango [", min, ", ", max, "]"
	
	Escribir "N�mero " Sin Saltar
	Leer nin
	
	Mientras (min <= nin) Y (nin <= max) Hacer
		cuenta <- cuenta + 1
		suma <- suma + nin
		Escribir "N�mero " Sin Saltar
		Leer nin
	FinMientras
	
	Escribir "## Fin, de proceso."
	Escribir "Numeros introducidos: ", cuenta
	Escribir "Suma de los numeros introducidos: ", suma
	
FinProceso
