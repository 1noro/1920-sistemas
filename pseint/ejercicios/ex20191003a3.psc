// detTriangulo ex20191003a

// lados de triangulo (equilatero ig, isosceles 2ig 1dif, escaleno dif)
Proceso detTriangulo
	Definir l1, l2, l3 Como Entero
	
	Escribir "lado 1 " Sin Saltar
	Leer l1
	Escribir "lado 2 " Sin Saltar
	Leer l2
	Escribir "lado 3 " Sin Saltar
	Leer l3
	
	Si ((l1 >= 1) Y (l1 <= 100)) Y ((l2 >= 1) Y (l2 <= 100)) Y ((l3 >= 1) Y (l3 <= 100)) Entonces
		Si (l1 >= (l2 + l3)) Y (l2 >= (l1 + l3)) Y (l3 >= (l1 + l2)) Entonces
			Si (l1 == l2) Y (l2 == l3) Entonces
				Escribir "[ OK ] Tri�ngulo equil�tero."
			SiNo
				Si (l1 == l2) O (l2 == l3) O (l1 == l3) Entonces
					Escribir "[ OK ] Tri�ngulo is�sceles."
				SiNo
					Escribir "[ OK ] Tri�ngulo escaleno."
				FinSi
			FinSi
		SiNo
			Escribir "[FAIL] No es un tri�ngulo."
		FinSi
	SiNo
		Escribir "[FAIL] Tods los valores deben estar en el rango [1, 100]."
	FinSi
	
FinProceso
