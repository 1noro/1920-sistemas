// matrices2 ex20191024b

// in <- 5 num int en [1, 99] gardar nunha matriz.
// luego se presentara:
// 		suma total
// 		promedio
//		o mais pequeno
//		o maior
Proceso matrices2
	Definir n, long, i, suma, min, max Como Entero
	long <- 5
	Dimension n[long]

	Para i <- 0 Hasta long - 1 Con Paso 1 Hacer
		Repetir
			Escribir "Introduce o ", i + 1, " n�mero en [1, 99] " Sin Saltar
			Leer n[i]
		Hasta Que 1 <= n[i] Y n[i] <= 99 
	FinPara
	
	// SUMA TOTAL
	suma <- 0
	min <- 100
	max <- 0
	Para i <- 0 Hasta 4 Hacer
		suma <- suma + n[i]
		Si n[i] < min Entonces
			min <- n[i]
		FinSi
		Si n[i] > max Entonces
			max <- n[i]
		FinSi
	FinPara
	
	// PRESENTAR RESULTADOS
	Escribir "Vector: [" Sin Saltar
	Para i <- 0 Hasta long - 1 Con Paso 1 Hacer
		Si i <> (long - 1) Entonces
			Escribir n[i], ", " Sin Saltar
		SiNo
			Escribir n[i], "]"
		FinSi
	FinPara
	
	Escribir "Suma total: ", suma
	Escribir "Promedio: ", suma / long
	Escribir "M�nimo: ", min
	Escribir "M�ximo: ", max
	
FinProceso
