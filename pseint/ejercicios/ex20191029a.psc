// matrices4 ex20191029a

// num filas en [1, 9]
// num columnas en [1, 9]
// meter en cada posicion da matriz un num random entre 1 e 30

// presentar contenido cudriculado con los espacios
Proceso matrices4
	
	Definir m, filas, columnas, ifila, icolumna, i Como Entero
	Definir c Como Cadena
	
	Repetir
		Escribir "N� de filas en [1, 9]: " Sin Saltar
		Leer filas 
	Hasta Que 1 <= filas Y filas <= 9 
	
	Repetir
		Escribir "N� de columnas en [1, 9]: " Sin Saltar
		Leer columnas 
	Hasta Que 1 <= columnas Y columnas <= 9 
	
	Dimension m[filas, columnas]
	
	Para ifila <- 0 Hasta filas - 1 Con Paso 1 Hacer
		Para icolumna <- 0 Hasta columnas - 1 Con Paso 1 Hacer
			m[ifila, icolumna] <- azar(29) + 1
		FinPara
	FinPara
	
	// PRIMERA LINEA
	Para i <- 0 Hasta columnas - 1 Con Paso 1 Hacer
		Escribir "---" Sin Saltar
	FinPara
	Escribir "-"
	
	// TABLA
	Para ifila <- 0 Hasta filas - 1 Con Paso 1 Hacer
		Para icolumna <- 0 Hasta columnas - 1 Con Paso 1 Hacer
			Si m[ifila, icolumna] < 10 Entonces
				c <- Concatenar(" ", ConvertirATexto(m[ifila, icolumna]))
			SiNo
				c <- ConvertirATexto(m[ifila, icolumna])
			FinSi
			Si icolumna = columnas - 1 Entonces
				Escribir "|", c, "|" Sin Saltar
			SiNo
				Escribir "|", c Sin Saltar
			FinSi
		FinPara
		Escribir ""
		
		// LINEA SEPARADORA
		Si ifila < filas - 1 Entonces
			Para i <- 0 Hasta columnas - 1 Con Paso 1 Hacer
				Si i = 0 Entonces
					Escribir "|--" Sin Saltar
				SiNo
					Escribir "+--" Sin Saltar
				FinSi
			FinPara
			Escribir "|"
		FinSi
		
	FinPara
	
	// �LTIMA LINEA
	Para i <- 0 Hasta columnas - 1 Con Paso 1 Hacer
		Escribir "---" Sin Saltar
	FinPara
	Escribir "-"
	
FinProceso
