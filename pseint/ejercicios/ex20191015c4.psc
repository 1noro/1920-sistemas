// factorizar ex20191015c4

// versi�n corregida (a�n es optimizable)
// estructura libre (Mientras, Para)
// representar a descomposici�n factorial
Proceso factorizar
	Definir num, i, inicio Como Entero
	Definir primero Como Logico
	
	Repetir
		Escribir "N�mero en [2, 1999] " Sin Saltar
		Leer num
	Hasta Que (num >= 2) Y (num <= 1999)

	inicio <- 2
	primero <- Verdadero
	Escribir num, " = " Sin Saltar
	Mientras num > 1 Hacer
		Para i <- inicio Hasta num Con Paso  1 Hacer
			Si (num MOD i) = 0 Entonces
				Si primero = Verdadero Entonces
					Escribir i Sin Saltar
					primero <- Falso
				SiNo
					Escribir " x ", i Sin Saltar
				FinSi
				num <- num / i
				i <- i - 1 // punto cr�tico
			FinSi
		FinPara
		inicio <- inicio + 1
	FinMientras
	
	Escribir ""
	
FinProceso