// mult7 ex20191010d

// para
// tabla de multiplicar del 7
Proceso mult7
	Definir c, i Como Entero
	c <- 7
	
	Para i <- 1 Hasta 10 Con Paso 1 Hacer
		Escribir c, " * ", i, " = ", (c * i) 
	FinPara
	
FinProceso
