// controlCualificacions ex20191002a

//selectivas dobres ani�adas (emulando selectiva multiple)
//1, 2: moi deficiente
//3, 4: insu
//5: sufi
//6: ben
//7, 8: notable
//9, 10: sobre
//oth: nota incorrecta
Proceso controlCualificacions
	Definir n Como Entero
	
	Escribir "Nota " Sin Saltar
	Leer n
	
	Si (n == 1) O (n == 2) Entonces
		Escribir "Moi deficiente."
	SiNo
		Si (n == 3) O (n == 4) Entonces
			Escribir "Insuficiente."
		SiNo
			Si n == 5 Entonces
				Escribir "Suficiente."
			SiNo
				Si n == 6 Entonces
					Escribir "Ben."
				SiNo
					Si (n == 7) O (n == 8) Entonces
						Escribir "Notable."
					SiNo
						Si (n == 9) O (n == 10) Entonces
							Escribir "Sobresaliente."
						SiNo
							Escribir "Nota incorrecta."
						FinSi
					FinSi
				FinSi
			FinSi
		FinSi
	FinSi
	
FinProceso
