// controlCualificacions ex20191002a2

//selectivas dobres ani�adas (emulando selectiva multiple)
//1, 2: moi deficiente
//3, 4: insu
//5: sufi
//6: ben
//7, 8: notable
//9, 10: sobre
//oth: nota incorrecta
Proceso controlCualificacions
	Definir n Como Entero
	
	Escribir "Nota " Sin Saltar
	Leer n
	
	Segun n Hacer
		1, 2:
			Escribir "Moi deficiente."
		3, 4:
			Escribir "Insuficiente."
		5:
			Escribir "Suficiente."
		6:
			Escribir "Ben."
		7, 8:
			Escribir "Notable."
		9, 10:
			Escribir "Sobresaliente."
		De Otro Modo:
			Escribir "Nota incorrecta."
	FinSegun
	
FinProceso
