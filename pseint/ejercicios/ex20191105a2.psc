// estaEnFibonacci ex20191105a2

// escribir Fibonacci hasta el limite impuesto por el numero buscado
Proceso estaEnFibonacci
	Definir b, n1, n2, aux Como Entero

	Repetir
		Escribir "Asignar l�mite superior a la secuencia de Fibonacci: " Sin Saltar
		Leer b 
	Hasta Que b >= 0

	n1 <- 0
	n2 <- 1
	aux <- 0
	Repetir
		Escribir aux
		aux <- n1 + n2
		n1 <- n2
		n2 <- aux
		Si b = aux Entonces
			aux <- b
		FinSi
	Hasta Que aux > b

FinProceso
