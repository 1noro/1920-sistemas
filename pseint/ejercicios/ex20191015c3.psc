// factorial ex20191015c3

// estructura libre (Para)
// representar a descomposición factorial
Proceso perimetroCadrado1
	Definir n, i, e, p Como Entero
	
	Repetir
		Escribir "Número en [2, 1999] " Sin Saltar
		Leer n
	Hasta Que (n >= 2) Y (n <= 1999)
	
	Escribir n, " = " Sin Saltar
	
	i <- 2
	p <- -1
	
	Para e <- n Hasta 0 Con Paso p Hacer
		
		Si ((n MOD i) == 0) Entonces
	        Escribir i Sin Saltar
	        n <- n / i
			Si n > 1 Entonces
				Escribir " x " Sin Saltar
			FinSi
			p <- 0
	    SiNo
			i <- i + 1
			p <- -1
		FinSi
		
	FinPara
	
	Escribir ""
	
FinProceso