// determinarIdade ex20190926d

// dia mes ano determinar a idade
Proceso determinarIdade
	Definir _d, _m, _a Como Entero
	Definir d, m, a Como Entero
	Definir edad Como Entero
	_d <- 26
	_m <- 9
	_a <- 2019
	
	Escribir "D�a de nacimiento " Sin Saltar
	Leer d
	Escribir "Mes de nacimiento " Sin Saltar
	Leer m
	Escribir "A�o de nacimiento " Sin Saltar
	Leer a
	
	edad <- _a - a
	
	Si m = _m Y d > _d  Entonces
		edad <- edad - 1
	SiNo
		Si m > _m Entonces
			edad <- edad - 1
		FinSi
	FinSi
	
	Escribir "Tienes ", edad, " a�os."
FinProceso
