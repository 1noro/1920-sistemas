// perimetroCadrado1 ex20191015b

// para
// perimetro cadrado de *, con dimensións entre [2, 20]
Proceso perimetroCadrado1
	Definir a, i, e Como Entero
	
	Repetir
		Escribir "Medida en [2, 20] " Sin Saltar
		Leer a
	Hasta Que (a >= 2) Y (a <= 20)
	
	Escribir ""
	
	Para i <- 1 Hasta a Con Paso 1 Hacer
		Para e <- 1 Hasta a Con Paso 1 Hacer
			Si (e == 1) O (e == a) O (i == 1) O (i == a) Entonces
				Escribir " *" Sin Saltar
			SiNo
				Escribir "  " Sin Saltar
			FinSi
		FinPara
		Escribir ""
	FinPara
	
	Escribir ""
	
FinProceso