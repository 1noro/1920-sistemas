// primo ex20191017a

// in <- n [2, 9999]
// out -> primo || no primo
// exit <- 0
Proceso detPrimo
	Definir n, i Como Entero
	Definir es_primo Como Logico
	
	Repetir
		Escribir "N�mmero " Sin Saltar
		Leer n
	Hasta Que 2 <= n Y n <= 9999 
	
	Para n <- 2 Hasta 100 Con Paso 1 Hacer
		
		es_primo <- Verdadero
		i <- 2
		Mientras i < n Hacer
			Si (n MOD i) = 0 Entonces
				es_primo <- Falso
				i <- n
			FinSi
			i <- i + 1
		FinMientras
		
		Si es_primo Entonces
			Escribir n, " es primo."
		SiNo
			Escribir n, " NO es primo."
		FinSi
		
	FinPara
	
FinProceso
