// factorizar ex20191016a

// solicitar nombre (cadena) y darle al usuario
// la opc�n de presentar ese nombre a mayusculas
// o min�sculas.

// el proces acabara cuando el usuario pulse
// intro en el nombre solicitado ""
Proceso mayusc
	Definir nombre Como Cadena
	Definir in Como Entero
	
	Repetir
		Escribir "Nombre (INTRO para salir) " Sin Saltar
		Leer nombre
		
		Si nombre <> "" Entonces
			
			Repetir
				Escribir "Convertir a MAY�SCULAS (1) o a min�sculas (2)? " Sin Saltar
				Leer in
			Hasta Que in == 1 O in == 2
			
			Si in = 1 Entonces
				Escribir Mayusculas(nombre)
			SiNo
				Escribir Minusculas(nombre)
			FinSi
			
		FinSi
	Hasta Que nombre == ""

FinProceso
	