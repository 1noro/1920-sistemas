// palindromo ex20191023a

// frase_ori <- frase
// out -> es palindromo o no

// ignorar acentos, mayusculas y espacios

// funcion util: concatenar()
// SIN HACER REVERSE
Proceso palindromo
	
	Definir frase_ori, frase_limpia Como Cadena
	Definir c Como Caracter
	Definir i Como Entero
	Definir es_palindromo Como Logico
	
	Repetir
		
		Escribir "Escribe una frase sin signos de puntuaci�n."
		Escribir "Frase " Sin Saltar
		Leer frase_ori
		
		Si frase_ori <> "" Entonces
			
			frase_limpia <- ""
			es_palindromo <- Verdadero
			
			Para i <- 0 Hasta Longitud(frase_ori) Con Paso 1 Hacer
				
				c <- Minusculas(SubCadena(frase_ori, i, i))
				
				Segun c Hacer
					"�":
						c <- "a"
					"�":
						c <- "e"
					"�":
						c <- "i"
					"�":
						c <- "o"
					"�":
						c <- "u"
					"�":
						c <- "u"
				FinSegun
				
				Si c <> " " Entonces
					frase_limpia <- Concatenar(frase_limpia, c)
				FinSi
			FinPara
			
			i <- 0
			Mientras es_palindromo Y i < trunc(Longitud(frase_limpia)/2) Hacer
				c <- SubCadena(frase_limpia, i, i)
				Si c <> SubCadena(frase_limpia, Longitud(frase_limpia) - i - 1, Longitud(frase_limpia) - i - 1) Entonces
					es_palindromo <- Falso
				FinSi
				i <- i + 1
			FinMientras
			
			Si es_palindromo Entonces
				Escribir "[ OK ] Es pal�ndromo :D"
			SiNo
				Escribir "[FAIL] NO es pal�ndromo :("
			FinSi
			
		FinSi
	Hasta Que frase_ori = ""
	
FinProceso
