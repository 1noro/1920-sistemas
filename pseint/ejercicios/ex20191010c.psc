// divisores enteros ex20191010c

// repetir
// in <- ent + [1, 100]
// imprimir sus divisores enteros
Proceso sumaNumeros
	Definir n, i Como Entero
	
	Repetir
		Escribir "N�mero [1, 100] " Sin Saltar
		Leer n
	Hasta Que (n > 0) Y (n <= 100)
	
	Escribir "# Divisores:" Sin Saltar
	
	i <- n 
	Repetir
		Si ((n MOD i) == 0) Entonces
			Escribir " ", i Sin Saltar
		FinSi
		i <- i - 1
	Hasta Que (i == 0)
	
	Escribir ""
	Escribir "Fin, de proceso."
	
FinProceso
