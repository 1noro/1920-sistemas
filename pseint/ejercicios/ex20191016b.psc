// mayusc ex20191016b

// solicitar nombre (cadena) y darle al usuario
// la opc�n de presentar ese nombre a mayusculas
// o min�sculas.

// el proces acabara cuando el usuario pulse
// intro en el nombre solicitado ""
Proceso mayusc
	Definir nombre Como Cadena
	Definir anterior Como Caracter
	Definir in, i Como Entero
	
	Repetir
		Escribir "Nombre (INTRO para salir) " Sin Saltar
		Leer nombre
		
		Si nombre <> "" Entonces
			
			Repetir
				Escribir " 1 - MAY�SCULAS"
				Escribir " 2 - min�sculas"
				Escribir " 3 - Tipo T�tulo"
				Escribir "Convertir a (1/2/3) " Sin Saltar
				Leer in
			Hasta Que in = 1 O in = 2 O in = 3
			
			Segun in Hacer
				1:
					Escribir Mayusculas(nombre)
				2:
					Escribir Minusculas(nombre)
				3:
					anterior <- " "
					Para i <- 0 Hasta Longitud(nombre) - 1 Con Paso 1 Hacer
						Si (Subcadena(nombre, i, i) <> " " O Subcadena(nombre, i, i) <> "_") Y (anterior = " " O anterior = "_") Entonces
							Escribir Mayusculas(Subcadena(nombre, i, i)) Sin Saltar
						SiNo
							Escribir Minusculas(Subcadena(nombre, i, i)) Sin Saltar
						FinSi
						anterior <- Subcadena(nombre, i, i)
					FinPara
					Escribir ""
			FinSegun
			
		FinSi
	Hasta Que nombre = ""
	
FinProceso
