// palindromo ex20191023a

// in <- frase
// out -> es palindromo o no

// ignorar acentos, mayusculas y espacios

// funcion util: concatenar()
Proceso palindromo
	
	Definir in, in2 Como Cadena
	Definir i Como Entero
	Definir c1, c2 Como Caracter
	Definir es_palindromo Como Logico
	
	Repetir
		
		Escribir "Escribe una frase sin signos de puntuaci�n"
		Escribir "Frase " Sin Saltar
		Leer in
		
		Si in <> "" Entonces
			
			in2 <- ""
			
			Para i <- 0 Hasta Longitud(in) Con Paso 1 Hacer
				es_palindromo <- Verdadero
				c1 <- SubCadena(in, i, i)
				
				Segun c1 Hacer
					"�":
						c1 <- "a"
					"�":
						c1 <- "e"
					"�":
						c1 <- "i"
					"�":
						c1 <- "o"
					"�":
						c1 <- "u"
					"�":
						c1 <- "u"
					"�":
						c1 <- "a"
					"�":
						c1 <- "e"
					"�":
						c1 <- "i"
					"�":
						c1 <- "o"
					"�":
						c1 <- "u"
					"�":
						c1 <- "u"
				FinSegun
				
				Si c1 <> " " Entonces
					in2 <- Minusculas(Concatenar(in2, c1))
				FinSi
			FinPara

			Para i <- 0 Hasta trunc(Longitud(in2)/2) Con Paso 1 Hacer
				c1 <- SubCadena(in2, i, i)
				c2 <- SubCadena(in2, Longitud(in2) - i - 1, Longitud(in2) - i - 1)

				Si c1 <> c2 Entonces
					es_palindromo <- Falso
				FinSi
			FinPara
			
			Si es_palindromo Entonces
				Escribir "[ OK ] Es pal�ndromo :D"
			SiNo
				Escribir "[FAIL] NO es pal�ndromo :("
			FinSi
			
		FinSi
	Hasta Que in = ""
	
FinProceso
