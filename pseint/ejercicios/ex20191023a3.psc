// palindromo ex20191023a

// in <- frase
// out -> es palindromo o no

// ignorar acentos, mayusculas y espacios

// funcion util: concatenar()
// SIN HACER REVERSE
Proceso palindromo
	
	Definir in, in2 Como Cadena
	Definir c Como Caracter
	Definir i Como Entero
	Definir es_palindromo Como Logico
	
	Repetir
		
		Escribir "Escribe una frase sin signos de puntuaci�n."
		Escribir "Frase " Sin Saltar
		Leer in
		
		Si in <> "" Entonces
			
			in2 <- ""
			
			Para i <- 0 Hasta Longitud(in) Con Paso 1 Hacer
				es_palindromo <- Verdadero
				c <- SubCadena(in, i, i)
				
				Segun c Hacer
					"�":
						c <- "a"
					"�":
						c <- "e"
					"�":
						c <- "i"
					"�":
						c <- "o"
					"�":
						c <- "u"
					"�":
						c <- "u"
					"�":
						c <- "a"
					"�":
						c <- "e"
					"�":
						c <- "i"
					"�":
						c <- "o"
					"�":
						c <- "u"
					"�":
						c <- "u"
				FinSegun
				
				Si c <> " " Entonces
					in2 <- Minusculas(Concatenar(in2, c))
				FinSi
			FinPara

			Para i <- 0 Hasta trunc(Longitud(in2)/2) Con Paso 1 Hacer
				c <- SubCadena(in2, i, i)
				Si c <> SubCadena(in2, Longitud(in2) - i - 1, Longitud(in2) - i - 1) Entonces
					es_palindromo <- Falso
				FinSi
			FinPara
			
			Si es_palindromo Entonces
				Escribir "[ OK ] Es pal�ndromo :D"
			SiNo
				Escribir "[FAIL] NO es pal�ndromo :("
			FinSi
			
		FinSi
	Hasta Que in = ""
	
FinProceso
