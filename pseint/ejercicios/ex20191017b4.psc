// primo ex20191017b

// in <- n [2, 9999]
// out -> primo || no primo
// exit <- 0
Proceso detPrimo
	Definir min, max, n, i Como Entero
	Definir es_primo Como Logico
	
	Repetir
		Escribir "Lim. inf. [2, 99999] " Sin Saltar
		Leer min
		Escribir "Lim. sup. [", min, ", 99999] " Sin Saltar
		Leer max
	Hasta Que min <= max Y 2 <= min Y max <= 99999
	
	Para n <- min Hasta max Con Paso 1 Hacer
		
		es_primo <- Verdadero
		i <- 2
		// muchas menos vueltas (para 11 hace 2)
		Mientras i < n Y i <= trunc(n^(1/2)) Hacer
			Si (n MOD i) = 0 Entonces
				es_primo <- Falso
				i <- n
			FinSi
			i <- i + 1
		FinMientras
		
		Si es_primo Entonces
			Escribir n, " es primo."
		SiNo
			Escribir n, " NO es primo."
		FinSi
		
	FinPara
	
FinProceso
