// matrices3 ex20191024c

// in <- 10 num en [1, 99]
Proceso matrices3
	
	Definir n, long, i, e, j, in Como Entero
	Definir duplicado Como Logico
	long <- 10
	Dimension n[long]
	
	i <- 0
	Repetir
		duplicado <- Falso
		Escribir "Escribe el n�mero ", i + 1, " en [1, 99] " Sin Saltar
		Leer in
		
		Si in >= 1 Y in <= 99 Entonces
			
			Si i <> 0 Entonces
				Para e <- 0 Hasta i - 1 Con Paso 1 Hacer
					Si in = n[e] Entonces
						duplicado <- Verdadero
					FinSi
				FinPara
			FinSi
			
			Si duplicado Entonces
				Escribir "N�mero dupliado."
			SiNo
				n[i] <- in
				i <- i + 1
			FinSi
			
		SiNo
			Escribir "N�mero fuera de rango."
		FinSi
		
//		Escribir "Vector: [" Sin Saltar
//		Para j <- 0 Hasta i - 1 Con Paso 1 Hacer
//			Si j <> (i - 1) Entonces
//				Escribir n[j], ", " Sin Saltar
//			SiNo
//				Escribir n[j], "]"
//			FinSi
//		FinPara
		
	Hasta Que i = long
	
	// PRESENTAR RESULTADOS
	Escribir "Vector: [" Sin Saltar
	Para i <- 0 Hasta long - 1 Con Paso 1 Hacer
		Si i <> (long - 1) Entonces
			Escribir n[i], ", " Sin Saltar
		SiNo
			Escribir n[i], "]"
		FinSi
	FinPara
	
FinProceso
