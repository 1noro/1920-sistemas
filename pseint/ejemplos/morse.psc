
Proceso morse1
	definir frase, letra como cadena
	definir i como entero

	escribir "frase: "
	leer frase

	letra <- ""

	i <- 0
	repetir
		letra <- ""
		repetir
			letra <- concatenar(letra, subcadena(frase, i, i))
			i <- i + 1
		hasta que subcadena(frase, i, i) = "/" O i > longitud(frase)
		
		segun letra hacer
			".-":
				escribir "a" sin saltar
			"-...":
				escribir "b" sin saltar
			"-.-.":
				escribir "c" sin saltar
			"-..":
				escribir "d" sin saltar
			".":
				escribir "e" sin saltar
			"..-.":
				escribir "f" sin saltar
			"--.":
				escribir "g" sin saltar
			"....":
				escribir "h" sin saltar
			"..":
				escribir "i" sin saltar
			".---":
				escribir "j" sin saltar
			"-.-":
				escribir "k" sin saltar
			".-..":
				escribir "l" sin saltar
			"--":
				escribir "m" sin saltar
			"-.":
				escribir "n" sin saltar
			"---":
				escribir "o" sin saltar
			".--.":
				escribir "p" sin saltar
			"--.-":
				escribir "q" sin saltar
			".-.":
				escribir "r" sin saltar
			"...":
				escribir "s" sin saltar
			"-":
				escribir "t" sin saltar
			"..-":
				escribir "u" sin saltar
			"...-":
				escribir "v" sin saltar
			".--":
				escribir "w" sin saltar
			"-..-":
				escribir "x" sin saltar
			"-.--":
				escribir "y" sin saltar
			"--..":
				escribir "z" sin saltar
		FinSegun
		
	Hasta que i >= longitud(frase)
	Escribir ""
finproceso 
	