Proceso PalabraMayor
	Definir frase como Cadena
	Definir palabra, palabraMax como Cadena
	Definir i, tam Como Entero
	
	Escribir "Frase: " Sin Saltar
	Leer frase
	
	tam <- longitud(frase)
	
	palabra <- ""
	palabraMax <- ""
	i <- 0
	Mientras (i < tam) Hacer
		Si (SubCadena(frase, i, i) != " ") Entonces
			palabra <- Concatenar(palabra, SubCadena(frase, i, i))
		FinSi
		
		Si (SubCadena(frase, i, i) = " ") O (i = (tam - 1)) Entonces
			Si (longitud(palabra) > longitud(palabraMax)) entonces
				palabraMax <- palabra
			FinSi
			palabra <- ""
		FinSi
		
		i <- i + 1
	FinMientras
	
	Escribir "La primera palabra mas larga es: ", palabraMax 
	
FinProceso
