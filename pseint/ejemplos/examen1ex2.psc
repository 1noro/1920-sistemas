Proceso encontrar_matriz
	definir matriz como entero
	Dimension matriz[3, 3]
	
	matriz[0, 0] <- 1
	matriz[0, 1] <- 2
	matriz[0, 2] <- 3
	matriz[1, 0] <- 4
	matriz[1, 1] <- 5
	matriz[1, 2] <- 6
	matriz[2, 0] <- 7
	matriz[2, 1] <- 8
	matriz[2, 2] <- 9
	
	definir a, e, busqueda como entero
	definir encontrado como logico
	
	Repetir
		escribir "numero (0 salir) " Sin Saltar
		leer busqueda
		
		si busqueda <> 0 entonces
			encontrado <- falso
			a <- 0
			mientras a < 3 y encontrado = Falso Hacer
				e <- 0
				mientras e < 3 y encontrado = falso hacer
					si matriz[a, e] = busqueda Entonces
						escribir "encontrado ", busqueda, " en ", a, " ", e
						encontrado <- Verdadero
					FinSi
					e <- e + 1
				FinMientras
				a <- a + 1
			FinMientras
		FinSi
	Hasta Que busqueda = 0
FinProceso
