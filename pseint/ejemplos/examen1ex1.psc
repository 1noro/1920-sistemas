Proceso mayuscula
	definir frase como cadena
	definir i Como Entero
	escribir "frase " sin saltar
	leer frase
	escribir "mayusculas: " sin saltar
	para i <- 0 hasta longitud(frase) - 1 con paso 1 hacer
		si subcadena(frase, i, i) <> minusculas(subcadena(frase, i, i)) Entonces
			escribir subcadena(frase, i, i), " " sin saltar
		FinSi
	FinPara
	escribir ""
FinProceso
