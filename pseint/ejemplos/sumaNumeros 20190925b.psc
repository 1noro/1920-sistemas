// sumaNumeros 20190925b
Proceso sumaNumeros
	Definir n1, n2 Como Entero
	Escribir "n1: " Sin Saltar
	Leer n1
	Escribir "n2: " Sin Saltar
	Leer n2
	Escribir n1, " + ", n2, " = ", n1 + n2
FinProceso
