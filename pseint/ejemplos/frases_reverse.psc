Proceso frase_reverse
	Definir fraseIn, fraseOut, aux como cadena
	Definir i como entero
	
	Escribir "frase: " Sin Saltar
	leer fraseIn
	
	fraseOut <- ""
	aux <- ""
	
	Para i <- 0 Hasta Longitud(fraseIn) Hacer
		si subcadena(fraseIn, i, i) = " " O i = Longitud(fraseIn) entonces
			fraseOut <- Concatenar(aux, Concatenar(" ", fraseOut))
			aux <- ""
		SiNo
			aux <- concatenar(aux, subcadena(fraseIn, i, i))
		FinSi
	FinPara
	
	Escribir fraseOut
	
FinProceso
