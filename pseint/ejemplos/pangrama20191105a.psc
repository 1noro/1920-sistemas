Proceso Pangrama
	
	Definir i, j Como Entero
	Definir Frase, Letras Como Cadena
	Definir Encontrado Como Logico
	
	Dimension Letras[27]
	
	Letras[0] <- "a"
	Letras[1] <- "b"
	Letras[2] <- "c"
	Letras[3] <- "d"
	Letras[4] <- "e"
	Letras[5] <- "f"
	Letras[6] <- "g"
	Letras[7] <- "h"
	Letras[8] <- "i"
	Letras[9] <- "j"
	Letras[10] <- "k"
	Letras[11] <- "l"
	Letras[12] <- "m"
	Letras[13] <- "n"
	Letras[14] <- "�"
	Letras[15] <- "o"
	Letras[16] <- "p"
	Letras[17] <- "q"
	Letras[18] <- "r"
	Letras[19] <- "s"
	Letras[20] <- "t"
	Letras[21] <- "u"
	Letras[22] <- "v"
	Letras[23] <- "w"
	Letras[24] <- "x"
	Letras[25] <- "y"
	Letras[26] <- "z"
	
	Escribir "Dime que frase quieres poner (solo aquella que sea menor o igual que 100)"
	Repetir 
		Leer Frase
	Hasta Que Longitud(Frase) <= 100
	
	Para i <- 0 Hasta 27 - 1 Hacer
		Encontrado <- Falso
		Para j <- 0 Hasta Longitud(Frase) - 1 Hacer
			Si Letras[i] = SubCadena(Frase,j,j) Entonces
				Encontrado <- Verdadero
			FinSi
		FinPara
		Si Encontrado = Falso Entonces
			i <- 26
		FinSi
	FinPara
	
	Si Encontrado = Falso Entonces
		Escribir "No es un pangrama"
	SiNo 
		Escribir "Es un pangrama"
	FinSi
	
FinProceso