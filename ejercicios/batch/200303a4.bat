@ECHO OFF
CHCP 1250 > NUL
CLS
ECHO Se va  arealizar una copia de seguridad...
PAUSE > NUL
REM si pones \ al final de la carpeta destino la crea si no existe
REM de otra forma deberíamos ejecutar MKDIR "C:\bkptxt\" antes.
XCOPY ".\*.txt" "C:\bkptxt\" /A /H /Y
REM /A - Copia solo archivos con el atributo de archivo establecido;
REM      no cambia el atributo.
REM /H - Copia archivos ocultos y también archivos del sistema.
REM /Y - Suprime la petición de confirmación de sobrescritura de un
REM      archivo de destino existente.
ATTRIB -A *.txt
ECHO La copia de seguriad ha concluido...
PAUSE > NUL
CHCP 850 > NUL
