@echo off
CHCP 1250 > nul

:MENU
    cls
    echo *************************
    echo **  ORDENAR ARTÍCULOS  **
    echo *************************
    echo.
    echo Ordenar por:
    echo.
    echo 1. CÓDIGO
    echo 2. DESCRIPCIÓN
	echo.
    echo 0. Salir
    echo.
goto :READOPT

:READOPT
    choice /C "012" /N /M "Pulsa una opción: "
    if ERRORLEVEL 3 (goto :OPT2MENU)
    if ERRORLEVEL 2 (goto :OPT1MENU)
    if ERRORLEVEL 1 (goto :FIN)
goto :MENU

:OPT1MENU
    cls
    echo Ordenar por código de forma:
    echo.
    echo A. Ascendente
    echo D. Descendente
    echo.
    echo X. Cancelar
    echo.
goto :OPT1READOPT

:OPT2MENU
    cls
    echo Ordenar por descripción de forma:
    echo.
    echo A. Ascendente
    echo D. Descendente
    echo.
    echo X. Cancelar
    echo.
goto :OPT2READOPT

:OPT1READOPT
    choice /C "XAD" /N /M "Pulsa una opción: "
    if ERRORLEVEL 3 (goto :OPT1ACTIOND)
    if ERRORLEVEL 2 (goto :OPT1ACTIONA)
    if ERRORLEVEL 1 (goto :MENU)
goto :OPT1READOPT

:OPT2READOPT
    choice /C "XAD" /N /M "Pulsa una opción: "
    if ERRORLEVEL 3 (goto :OPT2ACTIOND)
    if ERRORLEVEL 2 (goto :OPT2ACTIONA)
    if ERRORLEVEL 1 (goto :MENU)
goto :OPT2READOPT

:OPT1ACTIONA
    sort /+1 articulos.txt > ordenado.txt
goto :OPTFIN

:OPT1ACTIOND
    sort /+1 articulos.txt > ordenado.txt /R
goto :OPTFIN

:OPT2ACTIONA
    sort /+8 articulos.txt > ordenado.txt
goto :OPTFIN

:OPT2ACTIOND
    sort /+8 articulos.txt > ordenado.txt /R
goto :OPTFIN

:OPTFIN
    cls
    echo.
    echo Archivo "ordenardo.txt" generado correctamente.
    echo.
	echo Presinoa cualquier tecla para continuar...
	pause > nul
goto :MENU

:FIN
    echo Bye :(
    CHCP 850 > nul
::exit
