@echo off
chcp 1250 > nul
cls

echo Se va a realizar la copia de seguridad, pulse una tecla par continuar...
pause > nul

if not exist "C:\bkptxt\" mkdir "C:\bkptxt\"

::SI SE LE PONES UNA BARRA \ AL FINAL DEL DESTINO SI NO EXISTE YA LO CREA (no haría falta el comando anterior)
:: /i en xcopy no copia todos los archivos en uno, cuando no se pone la barra \ al final del directorio (estilo, "C:\bkptxt")
::xcopy ".\*.txt" "C:\bkptxt\" /M /H /Y
xcopy ".\*.txt" "C:\bkptxt\" /A /H /Y

echo Se termino de copiar los archivos, presione cualquier tecla para continuar...
pause > nul

attrib -A *.txt

echo Programa finalizado, presione cualquier tecla para continuar...
pause > nul
chcp 850 > nul
