@echo off
cls

echo Se va a realizar la copia de seguridad, pulse una tecla par continuar...
pause > nul

if not exist "C:\bkptxt\" mkdir "C:\bkptxt\"

xcopy ".\*.txt" "C:\bkptxt\" /M /H /Y

echo Se termino de copiar los archivos, presione cualquier tecla para continuar...
pause > nul

