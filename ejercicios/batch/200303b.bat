@echo off
CHCP 1250 > nul

:MENU
    cls
    echo ***************************
    echo **  INFORMACIÓN SISTEMA  **
    echo ***************************
    echo .
    echo 1. Presentar fecha y hora
    echo 2. Crear info.txt
    echo   - Sistema Operativo
    echo   - Versión del S.O.
    echo   - Etiqueta vol disco
    echo   - Usuario
    echo 9. Salir
    echo.
goto :READOPT

:READOPT
    set /p opt=Escribe una opción: 

    if %opt% EQU 1 (goto :OPT1)
    if %opt% EQU 2 (goto :OPT2)
    if %opt% EQU 9 (goto :FIN) else (goto :OPTERROR)
goto :FIN

:OPTERROR
    echo La opción %opt% no es válida vuelva a introducirla
goto :READOPT

:OPT1
    cls
    echo Fecha: %date%
    ::date /t
    echo Hora: %time%
    ::time /t
    echo.
    echo Pulse para continuar...
    pause > nul
goto :MENU

:OPT2
    cls
    :: comandos útiles par esta info
    ::set
    ::ver

    echo Sistema operativo: %OS% > info.txt
    echo. >> info.txt
    echo Versión del S.O.: >> info.txt
    ver >> info.txt
    echo. >> info.txt
    vol >> info.txt
    echo. >> info.txt
    echo Usuario: %USERNAME% >> info.txt

    cls
    echo.
    echo Se creó el archivo "info.txt" con el siguiente contenido:
    echo.
    type info.txt
    echo.
    echo Pulse para continuar...
    pause > nul
goto :MENU

:FIN
    ::echo Programa finalizado, presione cualquier tecla para salir...
    ::pause > nul
    echo Bye :(
    CHCP 850 > nul
::exit
