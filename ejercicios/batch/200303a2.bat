@echo off
cls

echo Se va a realizar la copia de seguridad, pulse una tecla par continuar...
pause > nul

xcopy ".\*.txt" "C:\bkptxt\" /A /H /Y

echo Se termino de copiar los archivos, presione cualquier tecla para continuar...
pause > nul

attrib -A *.txt

echo Programa finalizado, presione cualquier tecla para continuar...
pause > nul
