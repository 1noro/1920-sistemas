@ECHO OFF
CHCP 1250 > NUL
CLS

SET CONT=0
SET SUMA=0

:LEER
    SET ENTRADA=
    SET /P ENTRADA=Introduce un número:
    IF [%ENTRADA%]==[] (
        GOTO :RESULTADOS
    ) ELSE (
        SET /A SUMA=%SUMA%+%ENTRADA%
        SET /A CONT=%CONT%+1
    )
GOTO :LEER

:RESULTADOS
    ECHO Total de números introducidos: %CONT%
    ECHO Suma: %SUMA%
    SET /A MEDIA=%SUMA%/%CONT%
    ECHO Media: %MEDIA%
REM GOTO :FIN

:FIN
    ECHO Pulsa una tecla para continuar...
    PAUSE > NUL
    CHCP 850 > NUL
REM EXIT
