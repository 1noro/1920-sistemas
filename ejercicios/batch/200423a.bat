@echo off
rem test.bat ALFREDO RODRIGUEZ GARCIA 1o DAM
CHCP 1250 > nul

:MENU
    cls
    echo Programita
    echo.
    echo A. Alta
    echo L. Listar
    echo.
    echo X. Salir
    echo.
rem goto :OPTREADOPT

:OPTREADOPT
    choice /C "ALX" /N /M "Pulsa una opción: "
    if ERRORLEVEL 3 (goto :FIN)
    if ERRORLEVEL 2 (goto :LISTAR)
    if ERRORLEVEL 1 (goto :ALTA)
goto :OPTREADOPT

:ALTA
    cls
    set /p nombre=Escribe tu nombre:
    set /p edad=Escribe tu edad:
    echo %nombre% %edad% >> datos.txt
goto :MENU

:LISTAR
    type datos.txt
goto :MENU

:FIN
    ram echo.
    rem echo Fin del programa, pulse una tecla para salir...
    rem pause > nul
    CHCP 850 > nul
rem exit
