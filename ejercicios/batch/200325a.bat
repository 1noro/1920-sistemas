@echo off
CHCP 1250 > nul
set /a suma=0
set /a cont=0 
set /a min=0 
set /a max=0

:MAIN
    echo Introduce numeros hasta donde quieras.
    echo Pulsa ENTER para salir y ver el resultado de la suma.
    echo.
    set in=[]
    set /p in=Escribe numero (enter para acabar): 
    if %in% EQU [] (goto :RESULTADOS)
    set /a min=%in%
goto :PREGUNTA

:PREGUNTA
    set /a suma=%suma%+%in%
    set /a cont=%cont%+1
    if %in% LSS %min% (set /a min=%in%)
    if %in% GTR %max% (set /a max=%in%)

    set in=[]
    set /p in=Escribe numero (enter para acabar): 
    if %in% EQU [] (goto :RESULTADOS)
goto :PREGUNTA

:RESULTADOS
    set /a media=%suma%/%cont%
    echo.
    echo Numeros introducidos: %cont%
    echo Suma: %suma%
    echo Media: %media%
    echo Mínimo: %min%
    echo Máximo: %max%
goto :FIN

:FIN
    echo.
    echo Fin del programa
    pause > nul
    CHCP 850 > nul
rem exit
