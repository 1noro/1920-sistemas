@echo off
CHCP 1250 > nul

:COMPROBACIONES
	if not exist alumnos.txt (goto :ERRARCHIVO)
    if [%1] EQU [] (goto :ERRCOLUMNA)
	if %1 GTR 5 (goto :ERRNUMCOLUMNA)
	if %1 EQU 0 (goto :OPT0)
    if %1 EQU 1 (goto :OPT1)
    if %1 EQU 2 (goto :OPT2)
    if %1 EQU 3 (goto :OPT3)
    if %1 EQU 4 (goto :OPT4)
    if %1 EQU 5 (goto :OPT5)
goto :FIN

:ERRARCHIVO
    echo Falta el archivo alumnos.txt
goto :FIN

:ERRCOLUMNA
    echo Falta número de columna
goto :FIN

:ERRNUMCOLUMNA
    echo Número de columna incorrecto
goto :FIN

:OPT0
    type alumnos.txt
goto :FIN

:OPT1
    echo  Ordenado por número
    echo --------------------------------------------
    sort /+1 alumnos.txt
goto :FIN

:OPT2
    echo  Ordenado por primer apellido
    echo --------------------------------------------
    sort /+5 alumnos.txt
goto :FIN

:OPT3
    echo  Ordenado por segundo apellido
    echo --------------------------------------------
    sort /+15 alumnos.txt
goto :FIN

:OPT4
    echo  Ordenado por nombre
    echo --------------------------------------------
    sort /+25 alumnos.txt
goto :FIN

:OPT5
    echo  Ordenado por edad
    echo --------------------------------------------
    sort /+39 alumnos.txt
goto :FIN

:FIN
    rem echo Bye :(
    CHCP 850 > nul
rem exit
