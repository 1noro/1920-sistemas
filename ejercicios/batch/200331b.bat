rem multiplos_2.bat ALFREDO RODRIGUEZ GARCIA 1o DAM
@echo off
CHCP 1250 > nul

:MAIN
    cls
    echo Este programa presenta la lista de multiplos de
    echo un numero dentro de un intervalo solicitado.
    echo.
rem goto :GETNUM

:GETNUM
    set num=
    set /p num=Escribe numero (1, 9999): 
    if [%num%] EQU [] (echo No es un número. & goto :GETNUM)
    if %num% LSS 1 (echo %num% está fuera del límite. & goto :GETNUM)
    if %num% GTR 9999 (echo %num% está fuera del límite. & goto :GETNUM)
rem goto :GETLINF

:GETLINF
    set linf=
    set /p linf=Escribe el límite inferior (1, 9999): 
    if [%linf%] EQU [] (echo No es un número. & goto :GETLINF)
    if %linf% LSS 1 (echo %linf% está fuera del límite. & goto :GETLINF)
    if %linf% GTR 9999 (echo %linf% está fuera del límite. & goto :GETLINF)
rem goto :GETLSUP

:GETSUP
    set lsup=
    set /p lsup=Escribe el límite inferior (%linf%, 9999): 
    if [%lsup%] EQU [] (echo No es un número. & goto :GETSUP)
    if %lsup% LSS %linf% (echo %lsup% está fuera del límite. & goto :GETSUP)
    if %lsup% GTR 9999 (echo %lsup% está fuera del límite. & goto :GETSUP)
    set cont=%linf%
    echo.
    echo Los múltiplos de %num% en (%linf%, %lsup%) son:
rem goto :CALCULO

:CALCULO
    FOR /L %%G in (%linf%, 1, %lsup%) DO (call :FUN %%G)
rem goto :FIN

:FUN
    set /a resto=%1 %% %num%
    if %resto% EQU 0 (echo  %1)
goto :EOF

:FIN
    echo.
    echo Fin del programa, pulse una tecla para salir...
    pause > nul
    CHCP 850 > nul
rem exit

