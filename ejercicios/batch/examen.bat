@ECHO OFF
REM ALFREDO RODRIGUEZ GARCIA 1o DAM 20200429
CHCP 1250 > NUL

SET /P EXT=Extensi�n a copiar:

IF NOT EXIST "C:\backup\" MKDIR "C:\backup\"
ECHO Copia realizada el d�a %DATE% a las %TIME% > backup.log
ECHO --- Archivos copiados --- >> backup.log
FOR %%f IN ("*.%EXT%") DO (
    ECHO "%%f" >> backup.log
    REM Pongo el nul para que no se muestre el texto del comando copy
    COPY /Y "%%f" C:\backup\ > NUL
)

ECHO --- Archivos en el directorio --- >> backup.log
FOR %%f IN ("C:\backup\*") DO (
    ECHO "%%f" >> backup.log
)

:FIN
    ECHO Copia de seguridad concluida
    ECHO Pulsa una tecla para continuar...
    PAUSE > NUL
    CHCP 850 > NUL
REM EXIT
