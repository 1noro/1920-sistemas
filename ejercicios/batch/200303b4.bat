@ECHO OFF
CHCP 1250 > NUL

:MENU
    CLS
    REM Vaciamos la variable %OPT% para que no mantenga el valor anterior
    SET %OPT%=
    ECHO.
    ECHO Información del sistema
    ECHO.
    ECHO 1. Presentar fecha y hora.
    ECHO 2. Crear info.txt
    ECHO    - S.O.
    ECHO    - Versión S.O.
    ECHO    - Etiqueta vol. de disco.
    ECHO    - Usuario.
    ECHO 3. Salir.
    SET /P OPT=Introduce una opción:
    REM Comprobaciones previas:
    REM IF "%OPT%" EQU "" ( GOTO :NOOPT )
    IF %OPT% LSS 1 ( GOTO :NOOPT )
    IF %OPT% GTR 3 ( GOTO :NOOPT )
    REM Acciones:
    IF %OPT% EQU 1 ( GOTO :SHOWDATE )
    IF %OPT% EQU 2 ( GOTO :INFO )
    IF %OPT% EQU 3 ( GOTO :FIN )
GOTO :MENU

:NOOPT
    ECHO Opción no válida.
    ECHO.
    ECHO Pulsa una tecla para continuar...
    PAUSE > NUL
GOTO :MENU

:SHOWDATE
    REM DATE /T
    REM TIME /T
    ECHO %DATE% - %TIME%
    ECHO.
    ECHO Pulsa una tecla para continuar...
    PAUSE > NUL
GOTO :MENU

:INFO
    ECHO Informe del sistema > info.txt
    ECHO %OS% >> info.txt
    VER >> info.txt
    VOL >> info.txt
    ECHO %USERNAME% >> info.txt
    ECHO.
    ECHO Informe creado, pulsa una tecla para continuar...
    PAUSE > NUL
GOTO :MENU

:FIN
    CHCP 850 > NUL
REM EXIT
