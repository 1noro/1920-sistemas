@echo off
CHCP 1250 > nul
set /a suma=0
set /a cont=0 

:MAIN
    echo Introduce numeros hasta donde quieras.
    echo Pulsa ENTER para salir y ver el resultado de la suma.
    echo.
goto :PREGUNTA

:PREGUNTA
    set in=[]
    set /p in=Escribe numero (enter para acabar): 
    ::echo in: "%in%"
    if %in% EQU [] (goto :RESULTADOS)
    set /a suma=%suma%+%in%
    set /a cont=%cont%+1
goto :PREGUNTA

:RESULTADOS
    echo.
    echo Numeros introducidos: %cont%
    echo Suma: %suma%
goto :FIN

:FIN
    echo.
    echo Fin del programa
    pause > nul
    CHCP 850 > nul
::exit
