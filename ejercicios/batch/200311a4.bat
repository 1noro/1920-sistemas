@echo off
CHCP 1250 > nul

:MENU
    cls
    echo *************************
    echo **  ORDENAR ARTÍCULOS  **
    echo *************************
    echo.
    echo Ordenar por:
    echo.
    echo 1. CÓDIGO
    echo 2. DESCRIPCIÓN
	echo.
    echo 0. Salir
    echo.
rem goto :READOPT

:READOPT
    choice /C "012" /N /M "Pulsa una opción: "
    if ERRORLEVEL 3 (
        set optname=DESCRIPCIÓN
        set optad=8
        goto :OPTMENU
    )
    if ERRORLEVEL 2 (
        set optname=CÓDIGO
        set optad=1
        goto :OPTMENU
    )
    if ERRORLEVEL 1 (goto :FIN)
goto :MENU

:OPTMENU
    cls
    echo Ordenar por %optname% de forma:
    echo.
    echo A. Ascendente
    echo D. Descendente
    echo.
    echo X. Cancelar
    echo.
rem goto :OPTREADOPT

:OPTREADOPT
    choice /C "XAD" /N /M "Pulsa una opción: "
    if ERRORLEVEL 3 (
        set optord=/R
        goto :OPTACTION
    )
    if ERRORLEVEL 2 (
        set optord=
        goto :OPTACTION
    )
    if ERRORLEVEL 1 (goto :MENU)
goto :OPTREADOPT

:OPTACTION
    if exist ordenado.txt (
        choice /C "SN" /M "El archivo ya existe, quieres sobreescribirlo "
        if ERRORLEVEL 2 (goto :MENU)
    )
    sort /+%optad% articulos.txt %optord% > ordenado.txt
goto :OPTFIN

:OPTFIN
    echo.
    echo Archivo "ordenardo.txt" generado correctamente.
    echo.
	echo Presinoa cualquier tecla para continuar...
	pause > nul
goto :MENU

:FIN
    echo Bye :(
    CHCP 850 > nul
rem exit
