@echo off
CHCP 1250 > nul

:MENU
    cls
    echo **************************
    echo **  LISTA DE CONTACTOS  **
    echo **************************
    echo.
    echo 1. Orden alfabético [A-Z]
    echo 2. Orden alfabético [Z-A]
    echo 3. Orden edad [Menor-Mayor]
    echo 4. Orden edad [Mayor-Menor]
	echo.
    echo 5. Salir
    echo.
goto :READOPT

:READOPT
    choice /C "12345" /N /M "Pulsa una opción: "
	if ERRORLEVEL 5 (goto :FIN)
    if ERRORLEVEL 4 (goto :OPT4)
    if ERRORLEVEL 3 (goto :OPT3)
    if ERRORLEVEL 2 (goto :OPT2)
    if ERRORLEVEL 1 (goto :OPT1)
goto :MENU

:OPT1
    cls
    sort /+12 contactos.txt
    echo.
	echo Presinoa cualquier tecla para continuar...
	pause > nul
goto :MENU

:OPT2
    cls
    sort /+12 contactos.txt /R
    echo.
	echo Presinoa cualquier tecla para continuar...
	pause > nul
goto :MENU

:OPT3
    cls
    sort /+1 contactos.txt /R
    echo.
	echo Presinoa cualquier tecla para continuar...
	pause > nul
goto :MENU

:OPT4
    cls
    sort /+1 contactos.txt
    echo.
	echo Presinoa cualquier tecla para continuar...
	pause > nul
goto :MENU

:FIN
    echo Bye :(
    CHCP 850 > nul
::exit
